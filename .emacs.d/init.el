(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(org-fancy-priorities nasm-mode sudo-edit dockerfile-mode cql-mode nim-mode pyvenv lsp-pyright auctex lsp-haskell haskell-mode evil-commentary monokai-pro-theme gruber-darker-theme zenburn-theme writeroom-mode which-key projectile org-roam-ui websocket org-roam toc-org org-bullets neotree perspective magit yaml-mode python-mode rustic lsp-ui lsp-mode flycheck yasnippet highlight-escape-sequences highlight-numbers company-box company ivy-posframe smex ivy-rich counsel peep-dired dired-open all-the-icons-dired gruvbox-theme dashboard doom-modeline all-the-icons general evil-tutor evil-collection evil no-littering gcmh use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
