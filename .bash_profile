
# exports
export EDITOR="vim"
# nim-related
export PATH=$HOME/.nimble/bin:$PATH
#python exports
export PATH=$HOME/.local/bin:$PATH
# haskell exports
[ -f "$HOME/.ghcup/env" ] && source "$HOME/.ghcup/env" # ghcup-env


# source .bash_login
if [ -f ~/.bash_login ]; then
    . ~/.bash_login
fi
. "$HOME/.cargo/env"
